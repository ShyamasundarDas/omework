'use strict';

let form = document.querySelector('form');
let fields = form.querySelectorAll('.required');
let labelsError = form.querySelectorAll('.error-label');
let statusValid = {}; 
let btnBacket = document.querySelector('#btn-basket');
let countItems = document.querySelector('.fix-menu__basket-bage');


function handleInputs(event) {
  let value = event.target.value;
  let name = event.target.name;

  hideError(event.target);
  if (event.target.value !== '') {
    removeBorder(event.target);
  }
  localStorage.setItem(name, value);
}

function handleSubmit(event) {
  event.preventDefault();

  // поле Имя-Фамилия
  if (fields[0].value.length < 3) {
    labelsError[0].innerHTML = 'Введено меньше 2-х символов';
    setBorder(fields[0]);
    delete statusValid.nameUser;
  } else {
    hideError(labelsError[0]);
    statusValid.nameUser = 'true';
  }

  // рейтинг
  if (fields[1].value !== '') {
    labelsError[1].innerHTML = 'Оценка должна быть от 1 до 5';
  }
  if (fields[1].value > 0 && fields[1].value <= 5) {
    fields[1].value = Number(fields[1].value);
    hideError(labelsError[1]);
    statusValid.stars = 'true';
  } else {
    setBorder(fields[1]);
    labelsError[1].innerHTML = 'Оценка должна быть от 1 до 5';
    delete statusValid.stars;
  }

  // textarea
  if (fields[2].value === '') {
    labelsError[2].innerHTML = 'Введите данные';
    setBorder(fields[2]);
    delete statusValid.text;
  } else {
    hideError(labelsError[2]);
    statusValid.text = 'true';
  }

  if (Object.keys(statusValid).length === 3) {
    alert('Комментарий успешно отправлен');
    localStorage.clear();
    form.submit();
  }
}


function getItemsFromLocalStorage(){
    if(localStorage.getItem('items') !== null){
        countItems.classList.remove('hide');
        countItems.innerHTML = 1; 
        btnBacket.innerHTML = 'Товар уже в корзине';
        btnBacket.setAttribute('style', 'background:#888;border: 1px solid #888');
    }else{
        countItems.classList.add('hide');
        btnBacket.innerHTML = 'Добавить в корзину';
        btnBacket.setAttribute('style', '');
        return false;
    } 
}
getItemsFromLocalStorage();


function handleBacket(event){
    event.preventDefault();
    
    if(countItems.classList.contains('hide') || getItemsFromLocalStorage() === false){
        countItems.classList.remove('hide');
        countItems.innerHTML = localStorage.setItem('items', 1);
        countItems.innerHTML = 1;
        btnBacket.innerHTML = 'Товар уже в корзине';
        btnBacket.setAttribute('style', 'background:#888;border: 1px solid #888');
        localStorage.setItem('items', 1);
    }else{
        countItems.classList.add('hide');
        countItems.innerHTML = '';
        localStorage.clear();
        btnBacket.innerHTML = 'Добавить в корзину';
        btnBacket.setAttribute('style', '');
    }  
}


fields[0].value = localStorage.getItem('nameUser');
fields[1].value = localStorage.getItem('stars');
fields[2].value = localStorage.getItem('text');


form.addEventListener('input', handleInputs);
form.addEventListener('submit', handleSubmit);
btnBacket.addEventListener('click', handleBacket);


function setBorder(f){return f.classList.add('red-border')};
function removeBorder(f){return f.classList.remove('red-border')};
function hideError(f){ return f.innerHTML = ''};