'use strict';

// Задание №1
let arr;
/**
 * Получаем сумму значений массива
 * @param {array} arr 
 * @returns {number} sum // сумма значений массива
 */
function getNumbers(arr) {
    let sum = 0;
    for (let i = 0; i < arr.length; i++) {
      if(typeof arr[i] == `number`) sum += arr[i];
    }
    return sum;
  }
//console.log(getNumbers([1, 2, 'string', 4, 5, 6.5, {name: 'Коля'}, null]));


// Задание №2 в файле js/data.js


// Задание №3
let basket = [100, 110, 112];


/**
 * Добавление товара в корзину
 * @param {number} id // идентификатор товара
 * @returns {array}   // обновлённый массив
 */
function addToCart(id){
    if(basket.indexOf(id) !== -1){
        alert ('Данный товар уже находится в корзине');
    }else basket.push(id);
    return basket;   
}
console.log(addToCart(1001));



/**
 * Удаляет из карзины указанный товар
 * @param {number} id // идентификатор товара 
 * @returns {array}   // обновлённый массив 
 */
function removeItem(id){
    if(basket.indexOf(id)){
        basket.splice(basket.indexOf(id), 1);
    }
    return basket;    
}
console.log(removeItem(1001));