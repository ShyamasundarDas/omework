'use strict';

// Задание №2
let product = {
    name: 'Apple iPhone 13',
    colors: [
        'Красный',
        'Зелёный',
        'Розовый',
        'Бирюзовый',
        'Белый',
        'Чёрный'
    ],
    memory: [
        '128 Гб',
        '256 Гб',
        '512 Гб',
    ],
    screen: '6.1',
    operatingSystem: 'iOS 15',
    interfaces: 'NFC, Bluetooth, Wi-Fi',
    cpu: 'Apple A15 Bionic',
    weight: '173 г.',
    price: '75 990₽',
    discount: '8%',
    newPrice: '67 990₽',
    description: 'Наша самая совершенная система двух камер. Особый взгляд на прочность дисплея. Чип, с которым всё супербыстро.Аккумулятор держится заметно дольше. iPhone 13 - сильный мира всего.',
    imsges: [
        'images/image-1.webp',
        'images/image-2.webp',
        'images/image-3.webp',
        'images/image-4.webp',
        'images/image-5.webp'
    ],
    delivery: [
        {
            name: 'Самовывоз',
            date: 'Сегодня'
        },
        {
            name: 'Транспортной компанией',
            date: '7-10 дней' 
        },
        {
            name: 'Почтой России',
            date: '10-12 дней' 
        },
    ],
};


let reviewOne = {
    name: '<p class="bold">Марк Г.</p>',
    image: 'images/review-1.jpeg',
    rating: 5,
    comment: '<p><span class="bold">Опыт использования:</span> менее месяца</p><p><span class="bold">Достоинства:</span></p><p>это мой первый айфон после после огромного количества телефонов на андроиде. всёплавно, чётко и красиво. довольно шустрое устройство. камера весьма неплохая,ширик тоже на высоте.</p><p class="bold">Недостатки:</p><p>к самому устройству мало имеет отношение, но перенос данных с андроида - адскаявещь) а если нужно переносить фото с компа, то это только через itunes, которыйурезает качество фотографий исходное</p>'
};

let reviewTwo = {
    name: 'Валерий Коваленко',
    image: 'images/review-2.jpeg',
    rating: 4,
    comment: '<p><span class="bold">Опыт использования:</span> менее месяца</p><p><span class="bold">Достоинства:</span></p><p>OLED экран, Дизайн, Система камер, Шустрый А15, Заряд держит долго</p><p class="bold">Недостатки:</p><p>Плохая ремонтопригодность</p>'
};