'use strict';

// Задание №1
let a = '$100';
let b = '300$';
let summ = parseInt(a.slice(1, 4)) + parseInt(b.slice(0, 3));
console.log('Сумма равна = ' + '$100 ' + '+ 300$ = ' + summ + '$');

// Задание №2
let message = ' привет, медвед      ';
message = message.trim();
console.log(message[0].toUpperCase() + message.slice(1).toLowerCase());

// Задание №3
let age = prompt('Сколько вам лет?');
if(`${age}` <= 3){
    alert('младенец');
} else if (`${age}` > 3 && `${age}` <= 11) {
    alert('Вам ' + `${age}` + ' лет и вы ребенок');
}else if (`${age}` > 11 && `${age}` <= 18) {
    alert('Вам ' + `${age}` + ' лет и вы подросток');
}else if (`${age}` > 19 && `${age}` <= 40) {
    alert('Вам ' + `${age}` + ' лет и вы познаёте жизнь');
}else if (`${age}` > 41 && `${age}` <= 80) {
    alert('Вам ' + `${age}` + ' лет и вы познали жизнь');
}else if (`${age}` > 81) {
    alert('Вам ' + `${age}` + ' лет и вы долгожитель');
}


// Задание №4
let msg = 'Я работаю со строками как профессионал!';
function countWords(str) {
    return str.trim().split(/\s+/).length;
}
console.log('Количество слов в этой строке:', countWords(msg));