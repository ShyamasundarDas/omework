'use strict';

class Form {
    successMsg(){
        return alert('Комментарий успешно отправлен');
    }
}



class AddReviewForm extends Form {

    constructor(e){
        e.preventDefault();
        super(e);
        this.form = document.querySelector('form');
        this.fields = this.form.querySelectorAll('.required');
        this.labelsError = this.form.querySelectorAll('.error-label'); 
        this.form.addEventListener( 'submit', this.handleSubmit());
    }

    
    handleSubmit() {
        let statusValid = {};
        
        if(this.fields[0].value.length < 3) {
            this.labelsError[0].innerHTML = 'Введено меньше 2-х символов';
            this.setBorder(this.fields[0]);
             delete statusValid.nameUser; 
        }else {
            this.hideError(this.labelsError[0]);
            this.removeBorder(this.fields[0]);
            statusValid.nameUser = 'true';
        }

        // рейтинг
        if(this.fields[1].value !== '') {
            this.labelsError[1].innerHTML = 'Оценка должна быть от 1 до 5';
        }
        if(this.fields[1].value > 0 && this.fields[1].value <= 5) {
            this.fields[1].value = Number(this.fields[1].value);
            this.hideError(this.labelsError[1]);
            this.removeBorder(this.fields[1]);
            statusValid.stars = 'true';
        }else {
            this.setBorder(this.fields[1]);
            this.labelsError[1].innerHTML = 'Оценка должна быть от 1 до 5';
            delete statusValid.stars;
        }

        // textarea
        if(this.fields[2].value === '') {
            this.labelsError[2].innerHTML = 'Введите данные';
            this.setBorder(this.fields[2]);
            delete statusValid.text;
        }else {
            this.hideError(this.labelsError[2]);
            this.removeBorder(this.fields[2]);
            statusValid.text = 'true';
        }

        // если валидация пройдена - отправляем форму
        if(Object.keys(statusValid).length === 3){
            this.form.submit();
            let msg = new Form();
            msg.successMsg();
        }
    }

    setBorder(f){return f.classList.add('red-border')}
    removeBorder(f){return f.classList.remove('red-border')}
    hideError(f){return f.innerHTML = ''}
}





addEventListener('submit', e => {
    new AddReviewForm(e)
});


