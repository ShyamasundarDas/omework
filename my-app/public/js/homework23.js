'use strict';

// Задание №1
let sec = prompt('Введите количество сек.','');

/**
 * Определяет является ли переменная числом
 * @param {string} sec // входные данные
 * @returns {boolean} 
 */
function isNumeric(sec){
    return !isNaN(sec)
}

if(isNumeric(sec) !== true || sec === ''){
    console.log('Ошибка, введите число');
}
else{
    let intervalId = setInterval(function() {
        sec = Math.floor(sec);
        console.log('округляем до  ',sec);
        sec--;
        if(sec === 0){
            clearInterval(intervalId);
        }
    }, 1000);
}



// Задание №2
let start = new Date().getTime();// начало выполнения запроса
let promise = fetch('https://reqres.in/api/users');
promise
    .then((response) => {
        return response.json();
    })
    .then((response) => {
        let arr = response.data;// массив с данными пользователей
        let count = Object.keys(arr).length;// count objects in array
        console.log('Количечтво пользователей: ', count);

       for(let i = 0; i < count; i++) {
        console.log('- ' + arr[i].first_name +arr[i].last_name + ' (' + arr[i].email +')');
       }  
    })

    .catch(() => {
        console.log('Ошибка получения данных с сервера');
    });
    let end = new Date().getTime();// окончание выполнения запроса
    console.log('Запрос выполнился за: ', `${end - start}` + 'мс');