'use strict';

// Задание №1
let obj = {};
// obj.name = 'Miron is teacher'; // добавляем новое свойство в объект

/**
 * Проверяет наличие свойств у объекта
 * @param {object} obj // объект для проверки
 * @returns {boolean} // true - объект не имеет свойств; false - имеет свойства
 */
function isEmpty(obj) {
    return Object.keys(obj).length === 0;
}
//alert(isEmpty(obj));


// Задание №2 в файле js/data.js


// Задание №3
let salaries = {
    John: 100000,
    Ann: 160000,
    Pete: 130000
};

/**
 * Выводит зарплату сотрудников  
 * @param {number} percent // процент повышения зарплаты
 * @param {number} sum     // общая сумма зарплаты всех сотрудников
 */
function raiseSalary(percent){
    let sum = 0;
    for (let key in salaries){
        salaries[key] = salaries[key] + (percent * salaries[key] / 100);
        salaries[key] = Math.floor(salaries[key]);
        sum = sum + salaries[key];
        //console.log(salaries[key]); // вывод зарплаты для каждого сотрудника отдельно
    }
    console.log('Общая зарплата после повышения = ', sum);
}
raiseSalary(10);