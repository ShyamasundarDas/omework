'use strict';

// Задание №1
let a = '100px';
let b = '323px';
let result = parseFloat(a) + parseFloat(b);
console.log('Задание №1');
console.log('Сумма равна = ' + '100px ' + '+ 323px = ' + result);

// Задание №2
console.log('Задание №2');
console.log('Максимальное число = ', Math.max(10, -45, 102, 36, 12, 0, -1));

// Задание №3
let d = 0.111;
let e = 45.333333;
let f = 3;
let g = 400000000000000;
let h = '1' == 1;
console.log('Задание №3');
console.log('Округляем 0.111 до 1: ', Math.ceil(d));
console.log('Округляем 45.333333 до 45.3: ', e.toFixed(1));
console.log('Возвести в степень 5: ', Math.pow(f, 5));
console.log('Сокращённая запись числа 400000000000000: ', '1e14');
console.log('Значение булевой переменной: ', h);

// Задание №4
//При сложение 0.1 + 0.2 ответ будет 0.30000000000000004, что не равно 0.3
console.log(0.1 + 0.2);
console.log(0.3);
