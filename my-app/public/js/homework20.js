'use strict';
// Задание №1
let num = 20;
for(let i = 1; i <= num; i++){
    if(i % 2 === 0) {
        console.log(i);
    }
}


// Задание №2
let sum = 0;
let i = 1;
while(i <= 3) {
    let value = +prompt('Введите три числа','');
    if(!value){
        alert('Ошибка, вы ввели не число');
    } 
    i++;
    sum += value;
}
alert('Сумма: ' + sum);


// Задание №3
let month = +prompt('Введите номер месяца','');
let arr = ['январь', 'февраль', 'март', 'апрель', 'май', 'июнь', 'июль', 'август', 'сентябрь', 'октябрь', 'ноябрь', 'декабрь'];
function getNameOfMonth(month){
    if(!month || month > 12){
        alert('Ошибка, введите число от 1 до 12');
        return;
    }
    let num = month - 1;
    alert(arr[num] + ' это ' + month + ' месяц в году');  
}
getNameOfMonth(month);


// Задание №3-1
for(let s = 0; s < arr.length; s++){ 
    if(s === 9) continue;
    console.log(arr[s]);
}


// Задание №4
let result = (function () {
    // IIFE - это функция которая выполняется сразу же после того, как она была определена
    let value = 'Я переменная из IIFE функции';
    return value;
})();
console.log(result); 