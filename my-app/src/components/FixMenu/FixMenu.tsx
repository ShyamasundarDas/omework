import React from 'react';

function FixMenu(props) {
    return (
        <div className='fix-menu'>
			<div className='fix-menu__header'>
				<h1>
					<img src='images/logo-oak.png' alt='Логотип' />
					<span className='orange'>Мой</span>Маркет
				</h1>
				<div className='fix-menu__basket'>
					<div className='fix-menu__basket-bage hide'></div>
				</div>
			</div>
		</div>
    )}

    export default FixMenu;