import React from 'react';

function Footer(props) {
    return (
        <div className='footer'>
            <div className='footer__block-left'>
                <span className='bold'>© ООО «<span className='orange'>Мой</span>Маркет», 2018-2022</span><br /> 
                Для уточнения информации звоните по номеру <a href='tel:+7 900 000 0000'>+79000000000</a>,<br /> 
                а предложения по сотрудничеству отправляйте на почту <a href='mailto:partner@mymarket.com'>partner@mymarket.com</a>
            </div>
        </div>
    )}

export default Footer;