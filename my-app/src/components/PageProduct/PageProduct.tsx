import React from 'react';


function PageProduct(props) {
    return (
    <>
		<div className='parent'>
			<div className='parent__nav'>
				<a href='page-1.html'>Электроника</a> &gt;
				<a href='#'>Смартфоны и гаджеты</a> &gt;
				<a href='#'>Мобильные телефоны</a> &gt;
				Apple
			</div>

			<h1>Смартфон Apple iPhone 13</h1>
			<div className='parent__items'>
				<div><img src='images/image-1.webp' alt='Яблоко1' /></div>
				<div><img src='images/image-2.webp' alt='Яблоко2' /></div>
				<div><img src='images/image-3.webp' alt='Яблоко3' /></div>
				<div><img src='images/image-4.webp' alt='Яблоко4' /></div>
				<div><img src='images/image-5.webp' alt='Яблоко5' /></div>
			</div>

			<div className='container'>
				<div className='container__left'>
					<span className='title tm30'>Цвет товара синий</span>
					<div className='container__reviewe-block'>
						<button type='button' className='image'><img src='images/color-1.webp' alt='Яблоко-цвет-1' /></button>
						<button type='button' className='image'><img src='images/color-2.webp' alt='Яблоко-цвет-2' /></button>
						<button type='button' className='image'><img src='images/color-3.webp' alt='Яблоко-цвет-3' /></button>
						<button type='button' className='image'><img src='images/color-4.webp' alt='Яблоко-цвет-4' /></button>
						<button type='button' className='image'><img src='images/color-5.webp' alt='Яблоко-цвет-5' /></button>
						<button type='button' className='image'><img src='images/color-6.webp' alt='Яблоко-цвет-6' /></button>
					</div>
					<div className='row-column'>
						<span className='title tm30'>Конфигурация памяти 128 ГБ</span>
						<div className='btn-block'>
							<button type='button' className='btn-block__btn'>128ГБ</button>
							<button type='button' className='btn-block__btn'>256ГБ</button>
							<button type='button' className='btn-block__btn'>512ГБ</button>
						</div>
					</div>

					<div className='row-column'>
						<span className='title tm30'>Характеристики товара</span>
						<div className='list-circle'>
							<ul>
								<li>Экран: <span className='bold'>6.1</span></li>
								<li>Встроенная память: <span className='bold'>128 ГБ</span></li>
								<li>Операционная система: <span className='bold'>iOS 15</span></li>
								<li>Беспроводные интерфейсы: <span className='bold'>NFC, Bluetooth, Wi-Fi</span></li>
								<li>Процессор: <span className='blue bold'>Apple A15 Bionic</span></li>
								<li>Вес: <span className='bold'>173 г</span></li>
							</ul>
						</div>
						<br />
						<span className='title'>Описание</span>
						<p>Наша самая совершенная система двух камер.<br />
							Особый взгляд на прочность дисплея.<br />
							Чип, с которым всё супербыстро.<br />
							Аккумулятор держится заметно дольше.<br />
							<i>iPhone 13 - сильный мира всего</i>.</p>
						<p>Мы разработали совершенно новую схему расположения и развернули объективы на 45 градусов. Благодаря этому внутри корпуса поместилась наша лучшая система двухкамер с увеличенной матрицей широкоугольной камеры. Кроме того, мы освободилиместо для системы оптической стабилизации изображения сдвигом матрицы. Иповысили скорость работы матрицы на сверхширокоугольной камере.</p>
						<p>Новая сверхширокоугольная камера видит больше деталей в тёмных областяхснимков. Новая широкоугольная камера улавливает на 47% больше света для болеекачественных фотографий и видео. Новая оптическая стабилизация со сдвигомматрицы обеспечит чёткие кадры даже в неустойчивом положении.</p>
						<p>Режим «Киноэффект» автоматически добавляет великолепные эффекты перемещенияфокуса и изменения резкости. Просто начните запись видео. Режим «Киноэффект»будет удерживать фокус на объекте съёмки, создавая красивый эффект размытиявокруг него. Режим «Киноэффект» распознаёт, когда нужно перевести фокус на другогочеловека или объект, который появился в кадре. Теперь ваши видео будут смотретьсякак настоящее кино.</p>
					</div>

					<div className='row-column tm30 hidden-sm'>
						<span className='bold'>Сравнение моделей</span>
						<table className='table'>
							<thead>
								<tr>
									<th>Модель</th>
									<th>Вес</th>
									<th>Высота</th>
									<th>Ширина</th>
									<th>Толщина</th>
									<th>Чип</th>
									<th>Объём памяти</th>
									<th>Аккумулятор</th>
								</tr>
							</thead>
							<tbody>
								<tr>
									<td>Iphone11</td>
									<td>194 грамма</td>
									<td>150.9 мм</td>
									<td>75.7 мм</td>
									<td>8.3 мм</td>
									<td>A13</td>
									<td>Bionicchipдо 128 Гб</td>
									<td>до 17 часов</td>
								</tr>
								<tr>
									<td>Iphone 12</td>
									<td>164 грамма</td>
									<td>146.7 мм</td>
									<td>71.5 мм</td>
									<td>7.4 мм</td>
									<td>A14</td>
									<td>Bionicchipдо 256 Гб</td>
									<td>до 19 часов</td>
								</tr>
								<tr>
									<td>Iphone 13</td>
									<td>174 грамма</td>
									<td>146.7 мм</td>
									<td>71.5 мм</td>
									<td>7.65 мм</td>
									<td>A15</td>
									<td>Bionicchipдо 512 Гб</td>
									<td>до 19 часов</td>
								</tr>
							</tbody>
						</table>
					</div>

					<div className='row-column tm30'>
						<div className='feedback-counter'>
							<h2>Отзывы <span className='grey'>425</span></h2>
						</div>
					</div>

					<div className='feedbacks'>
						<div className='feedbacks__feed'>
							<div className='feedbacks__reviewe-photo'>
								<img src='images/review-1.jpeg' alt='Марк Г.' className='img-circle' />
							</div>
							<div>
								<p><span className='bold'>Марк Г.</span></p>
								<img src='images/star-5.png' alt='рейтинг' className='stars' />
								<p><span className='bold'>Опыт использования: </span>менее месяца</p>
								<p><span className='bold'>достоинства:</span></p>
								<p>это мой первый айфон после после огромного количества телефонов на андроиде. всёплавно, чётко и красиво. довольно шустрое устройство. камера весьма неплохая,ширик тоже на высоте.</p>
								<p><span className='bold'>Недостатки:</span></p>
								<p>к самому устройству мало имеет отношение, но перенос данных с андроида - адскаявещь) а если нужно переносить фото с компа, то это только через itunes, которыйурезает качество фотографий исходное</p>
							</div>
						</div>
						<div className='feedbacks__feed'>
							<div className='feedbacks__reviewe-photo'>
								<img src='images/review-2.jpeg' alt='Валерий Коваленко' className='img-circle' />
							</div>
							<div>
								<p><span className='bold'>Валерий Коваленко</span></p>
								<img src='images/star-4.png' alt='рейтинг' className='stars' />
								<br />
								<p><span className='bold'>Опыт использования: </span>менее месяца</p>
								<p><span className='bold'>достоинства:</span></p>
								<p>OLED экран, Дизайн, Система камер, Шустрый А15, Заряд держит долго</p>
								<p><span className='bold'>Недостатки:</span></p>
								<p>Плохая ремонтопригодность</p>
							</div>
						</div>
					</div>

					<div className='feedback-form'>
						<form method='post' action='#' className='form'>
							<span className='title'>Добавить свой отзыв</span>
							<div className='feedback-form__row tm20'>
								<div className='feedback-form__wrap'>
									<label className='error-label'></label>
									<input type='text' name='nameUser' className='feedback-form__input required' placeholder='Ваше имя' />
								</div>
								<div className='feedback-form__wrap'>
									<label className='error-label'></label>
									<input type='number' name='stars' className='feedback-form__input required' placeholder='Оценка' />
								</div>
							</div>
							<div className='feedback-form__row tm20'>
								<label className='error-label'></label>
								<textarea className='feedback-form__textarea required' name='text' placeholder='Текст отзыва'></textarea>
							</div>
							<button type='submit' className='feedback-form__btn tm30' form='form'>Отправить отзыв</button>
						</form>
					</div>
				</div>

				<div className='container__right'>
					<div className='container__price'>
						<div className='row'>
							<span className='bold nowrap'>75 990₽</span>
							<span className='sale'>-8%</span>
							<div className='container__pull-right'>
								<div className='heart-icon'></div>
							</div>
						</div>
						<div className='row'>
							<h2>67 990₽</h2>
						</div>
						<div className='row-column'>
							<span>Самовывоз в четверг, 1 сентября — <span className='bold'>бесплатно</span></span>
							<span>Курьером в четверг, 1 сентября — <span className='bold'>бесплатно</span></span>
						</div>
						<button type='button' className='container__btn-orange tm30'>Добавить в корзину</button>
					</div>

					<div className='row-column'>
						<div className='title-two'>Реклама</div>
						<div className='info-bloks'>
							<span>Здесь могла бы быть ваша реклама</span>
						</div>
						<div className='info-bloks'>
							<span>Здесь могла бы быть ваша реклама</span>
						</div>
					</div>
				</div>

			</div>
			<div className='upToTop'><a href='#top'>Наверх</a></div>
		</div>
	</>
)}

export default PageProduct;