import './App.css'; // общие стили для всех страниц
import PageProduct from './components/PageProduct/PageProduct.tsx';
import Footer from './components/Footer/Footer.tsx';
import FixMenu from './components/FixMenu/FixMenu.tsx';
import './components/PageProduct/PageProduct.css';
import './components/Footer/Footer.css';
import './components/FixMenu/FixMenu.css';


function App() {
  return (
	<body>
		<FixMenu />
		<PageProduct />
		<Footer />
	</body>
  );
}

export default App;